const Koa = require('koa')
const Router = require('koa-router')
const file  = require('./models/from_file.js')
const myQuery = require('./models/from_database')
const app = new Koa()
const router = new Router()

router.get('/from_database', async (ctx,next)=>{
   ctx.body=myQuery.rows
   await next()
  })
  
  router.get('/from_file', async (ctx,next)=>{
   const data = await file
   ctx.body= data
   await next()
  })

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)