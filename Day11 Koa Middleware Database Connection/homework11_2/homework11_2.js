'use strict'
const fs = require('fs')
const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const serve = require('koa-static')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

let read = ()=>{
  return new Promise((resolve,reject)=>{
    fs.readFile('./public/homework2_1.json','utf8',(err,data)=>{
      if(err){
        reject(err)
      }
      else{
        let result = JSON.parse(data)
        resolve(result)
      }
    })

  })
}


router.get('/from_database', async (ctx,next)=>{
  const connection = await mysql.createConnection({
    host: 'localhost',
    user: 'root',
   password:'',
    database: 'codecamp'
  })
 const[rows,fields]= await connection.query('SELECT * FROM user')
 ctx.variable ='database'
 ctx.body=rows
 await next()
})

router.get('/from_file', async (ctx,next)=>{
 const data = await read()
 ctx.body= data
 ctx.variable='file'
 await next()
})

let middlewareDatabase = async (ctx,next)=>{
  const connection = await mysql.createConnection({
    host: 'localhost',
    user: 'root',
   password:'',
    database: 'codecamp'
  })
 const[rows,fields]= await connection.query('SELECT * FROM user')
 
 if(ctx.variable==='database')
 {
    let result =''
    let result2=''
    let date =new Date()
   rows.forEach(rows => {
     
    let changeDatabase =`{"id":${rows.id},"firstname":"${rows.firstname}","lastname":"${rows.lastname}",
    "salary":${rows.salary},"role":"${rows.role}"},`
    let changeDatabase2 =`userId:1,dateTime:'${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} ${date.getHours()}
                          -${date.getMinutes()}-${date.getSeconds()}',`
    result+=changeDatabase;
    result2+=changeDatabase2;
   })
   let newResult = result.substr(0,result.length-1)
   let newResult2 = result2.substr(0,result2.length-1)
   let obj = `{data:[${newResult}],additionalData:${newResult2}}`
   ctx.body = obj
 }
 else{
  await next()
 }
}

let middlewareFile = async (ctx,next)=>{
  if(ctx.variable==='file'){
let data= await read()
let date = new Date()
let result=''
let result2=''
data.forEach(data=>{
  let changeFile = `{"id":${data.id},"firstname":"${data.firstname}","lastname":"${data.lastname}",
                    "company": "${data.company}","salary":${data.salary}},`
  let changeFile2 = `userId:1,dateTime:'${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} ${date.getHours()}
                  -${date.getMinutes()}-${date.getSeconds()}',`
result+=changeFile
result2+=changeFile2              
})
let newResult = result.substr(0,result.length-1)
let newResult2 = result2.substr(0,result2.length-1)
let obj =`data:[${newResult}],additionalData:{${newResult2}}`
ctx.body=obj
}
else(
  await next()
)
}

app.use(middlewareDatabase)
app.use(middlewareFile)
app.use(serve(path.join(__dirname,'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)