const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()
const path = require('path') 
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'homework11_1',
    viewExt: 'ejs',
    cache: false,
  });
  
  router.get('/', async function (ctx) {
   const connection = await mysql.createConnection({
    host     : 'localhost',
    database : 'codecamp',
    user     : 'root',
    password : ''
});
const[rows,field] = await connection.query('SELECT * FROM user')
const dataEJS ={'data':rows}

await ctx.render('homework11_1',dataEJS)

  });
  
  app.use(router.routes())
  app.use(router.allowedMethods())
  app.listen(3000)