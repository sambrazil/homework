const mysql = require('mysql2/promise')
const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'homework12_1',
    viewExt: 'ejs',
    cache: false,
  });
router.get('/',async ctx=>{
 const connection = await mysql.createConnection({
     host:"localhost",
     user:"root",
     password:'',
     database:"codecamp"
 })
 const[employee,fields] = await connection.query("SELECT * FROM employee")
 const[book,fields2] = await connection.query("SELECT * FROM book")
 const[sell,fields3] = await connection.query("SELECT * FROM sell")
 const dataEJS ={"employee":employee,"book":book,"sell":sell}
 await ctx.render('homework12_1',dataEJS)
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)