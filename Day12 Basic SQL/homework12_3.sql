1.ค้นหาหนังสือจากชื่อ เช่น The Marquis and I ให้ค้นว่า Mar ก็ต้องหาเจอ
select * from book where title like '%Mar%';

2.ดึงหนังสือมาแสดง 2 เล่มแรก ที่ชื่อมีตัวอักษร o
select * from book  where title like '%o%' limit 2;

3.เพิ่มรายการขายหนังสือ 10 รายการ
insert into book (isbn,title,price) values ('0000','Tsubasa',10), ('1111','Doraemon',20), ('2222','Dragon ball',30), 
('3333','Let and go',40), ('4444','Naruto',50), ('5555','One piece',60), ('6666','Hunter x hunter',70), 
('7777','Digimon',80), ('8888','Double dragon',90), ('9999','Contra',100);      
 

4.หาว่าขายหนังสือได้กี่เล่ม
select sum(number) as SumBookNumber from sell;

5.หาว่ามีหนังสืออะไรบ้างที่ขายออก (แสดงแค่ ISBN ของหนังสือ)
select isbn from sell where number>0;

6.หาว่าขายหนังสือได้เงินทั้งหมดเท่าไร
select sum(number*price) as TotalValue from sell;