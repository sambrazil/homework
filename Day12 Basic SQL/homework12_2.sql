1.พนักงานคนที่ 5 ลาออก ลบข้อมูลคนที่ 5 ออก
delete from employee where id=5;

2.เจ้าของร้านอยากเก็บที่อยู่พนักงานเพิ่ม ใส่ที่อยู่ให้พนักงานแต่ละคน คนไหนไม่รู้ให้เป็น NULL
alter table employee add column address varchar(255);

3.หาจำนวนพนักงานทั้งหมดในร้าน
select count(*) from employee;

4.แสดงรายชื่อพนักงานที่อายุน้อยกว่า 20 ปี
select * from employee where age<20;

5.แสดงรายชื่อพนักงานทั้งหมดโดยให้แสดงแค่ field ชื่อจริง และ อายุ
select firstname,lastname,age from employee;

6.แก้ไขชื่อจริงของพนักงานที่ชื่อ Noah ให้เป็น Neil
update employee set firstname='Neil' where firstname='Noah';