const mysql = require('mysql2/promise')
const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')

const app = new Koa()
const router =new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
  });
  
  router.get('/',async ctx=>{
  const connection =await mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'day13'
  })    
const[instructor,fields] = await connection.query("SELECT i.id, i.name as instructor_name, c.name as course_name FROM instructors i LEFT JOIN courses c ON i.id=c.teach_by WHERE c.teach_by is null")  
const[course,fields2]  = await connection.query("SELECT c.id, c.name as course_name, i.name as instructor_name FROM courses c LEFT JOIN instructors i ON c.teach_by=i.id WHERE i.id is null")                                          
const dataEJS = {'data':instructor,'data2':course}
await ctx.render('template',dataEJS)

  })
  

  app.use(router.routes())
  app.use(router.allowedMethods())
  app.listen(3000)