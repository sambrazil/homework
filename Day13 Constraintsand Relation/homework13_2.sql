1.เพิ่ม student ลงไป 10 คน
insert into students (name) values('A'),('B'),('C'),('D'),('E'),('F'),('G'),('H'),('I'),('J');

2.ให้ student ลงเรียนแต่ละวิชาที่แตกต่างกัน (อาจจะซ้ำกันบ้าง)
insert into enrolls (student_id,course_id) values(1,1),(2,1),(3,2),(4,2),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10);

3.มีคอร์สไหนบ้างที่มีคนเรียน (ห้ามแสดงชื่อคอร์สซ้ำ)
select distinct e.course_id, c.name from enrolls e inner join courses c on  e.course_id = c.id;

4.มีคอร์สไหนบ้างที่ไม่มีคนเรียน
select c.id, c.name, e.course_id from courses c left join enrolls e on c.id = e.course_id where e.course_id is null;  
