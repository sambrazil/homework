1.หาว่ามีคอร์สไหนบ้างที่มีคนเรียน และให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น
select distinct e.course_id, c.name as course_name, i.name as instructor_name, c.price from enrolls e 
inner join courses c on e.course_id = c.id
inner join instructors i on c.teach_by = i.id;

2.หาว่ามีคอร์สไหนบ้างที่ไม่มีคนเรียน และให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น
select c.id, c.name as course_name, i.name as instructor_name, c.price from enrolls e
right join courses c on e.course_id = c.id
left join  instructors i on c.teach_by = i.id
where e.course_id is null;


3.ต่อจากข้อบนเพิ่มเงื่อนไขคือให้เอามาเฉพาะคอร์สที่มีคนสอน
select c.id, c.name as course_name, i.name as instructor_name, c.price from enrolls e
right join courses c on e.course_id = c.id
inner join  instructors i on c.teach_by = i.id
where e.course_id is null;
