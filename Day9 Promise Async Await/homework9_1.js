'use strict'
const fs = require('fs')
let read1 = ()=>{
    return new Promise((resolve,reject)=>{
        fs.readFile('head.txt','utf8',(err,data)=>{
           if(err){
               reject(err)
           }
           else{
            resolve(data)
           }          
        })
    })
};
let read2 = ()=>{
    return new Promise((resolve,reject)=>{
        fs.readFile('body.txt','utf8',(err,data)=>{
           if(err){
               reject(err)
           }
           else{
            resolve(data)
           }          
        })
    })
};
let read3 = ()=>{
    return new Promise((resolve,reject)=>{
        fs.readFile('leg.txt','utf8',(err,data)=>{
           if(err){
               reject(err)
           }
           else{
            resolve(data)
           }          
        })
    })
};
let read4 = ()=>{
    return new Promise((resolve,reject)=>{
        fs.readFile('feet.txt','utf8',(err,data)=>{
           if(err){
               reject(err)
           }
           else{
            resolve(data)
           }          
        })
    })
};
let write = (result)=>{
    return new Promise((resolve,reject)=>{
        fs.writeFile('robot.txt',result,'utf8',(err,data)=>{
           if(err){
               reject(err)
           }
           else{
            resolve()
           }          
        })
    })
};
let robot = async ()=>{
    try{
        let data1 = await read1()
        let data2 = await read2()
        let data3 = await read3()
        let data4 = await read4()
        console.log(data1)
        console.log(data2)
        console.log(data3)
        console.log(data4)
        let result = data1+'\r\n'+data2+'\r\n'+data3+'\r\n'+data4
        await write(result)
    }
    catch(err){
       console.error(err)
    }
}
robot();