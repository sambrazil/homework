const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const serve = require('koa-static')
const render =  require('koa-ejs')

const app =new Koa()
const router = new Router()

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
});

router.get('/',async ctx=> {
  await ctx.render('landing');
});

router.get('/about',ctx=>{
    ctx.body="Hello World"
})


app.use(serve(path.join(__dirname,'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)