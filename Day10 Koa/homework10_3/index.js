'use strict'
const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
   
  })
 let read = ()=>{
   return new Promise((resolve,reject)=>{
     fs.readFile('./public/homework2_1.json','utf8',(err,data)=>{
       if(err){
        reject(err)
       }
       else {
         const arr = JSON.parse(data)
        resolve(arr)
       }
      
     })
   })
 }
  
    router.get('/', async ctx => {
      const object = await read()
        const dataEJS = {
          "data":object
          
        }
      
      await ctx.render('template',dataEJS)
      
     })
  
app.use(serve(path.join(__dirname,'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)