let employees;
fetch('homework2_1.json').then(response => {
   return response.json();
})
.then(myJson => {
 employees = myJson;

 function addYearSalary(row) 
{
   row.yearSalary = row.salary*12  // add new key yearSalary

}

   function addNextSalary(row)
{
   let nextSalary = row.salary; 
    row.nextSalary=[]; //add  new key nextSalary
   for(let i=0;i<3;i++)
   {
    
       row.nextSalary.push(nextSalary);
       nextSalary= nextSalary+(nextSalary*0.1) //increase salary 10%
   }

}

 for(let i in employees)
 {
    addYearSalary(employees[i]); //send value to function addYearSalary
   
 }
 for(let i in employees)
 {
    addNextSalary(employees[i]); //send value to function addNextSalary
 }

 //print HTML Output
document.write('<table align="center" border=1>')
//Table Header
for(let i in employees[0])
{
   document.write('<th>'+i+'</th>')
}
//Table Data
for(let i in employees)
{
document.write('<tr><td>'+employees[i].id+'</td>') //id
document.write('<td>'+employees[i].firstname+'</td>') //firstname
document.write('<td>'+employees[i].lastname+'</td>') //lastname
document.write('<td>'+employees[i].company+'</td>') //company
document.write('<td>'+employees[i].salary+'</td>') //salary
document.write('<td>'+employees[i].yearSalary+'</td>') //yearSalary 
document.write('<td>')
for(let j=0;j<employees[i].nextSalary.length;j++)
 {
   let number = j+1;
   document.write(number+'. '+employees[i].nextSalary[j]+'<br>') //nextSalary 
 }
 document.write('</td></tr>')

}      
document.write('</table>')
})
.catch(error => {
   console.error('Error:', error);
});

