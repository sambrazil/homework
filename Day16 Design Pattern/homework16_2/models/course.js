module.exports = {
tableCourse(row){
 if(!row.id){
     return {}
 }
 else if(!row.price){
     return{}
 }
 return{
     id:row.id,
     name:row.name,
     detail:row.detail,
     price:row.price,
     teach_by:row.teach_by,
     created_at:row.created_at
 }
}, 
  async findByIdCourses(pool, id){
     const [rows] = await pool.query('select * from courses where id = ?',[id])
     return rows.map(this.tableCourse)
  },
async findByPrice(pool, price){
  const [rows] = await pool.query('select * from courses where price = ?',[price])
  return rows.map(this.tableCourse)
}
   
}