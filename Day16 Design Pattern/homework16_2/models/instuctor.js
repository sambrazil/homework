module.exports = {
    tableInstructors(row){
    if(!row.id)
    return {};

return{
  id:row.id,
  name:row.name,
  created_at:row.created_at
        }
    },

     
   async findAll(pool){
    const [rows] = await pool.query('select * from instructors')
    return rows.map(this.tableInstructors)
   },
   
  async findById(pool, id){
      const [rows] = await pool.query('select * from instructors where id = ?',[id])
      return rows.map(this.tableInstructors)
  }
}