const Koa = require('koa')
const Router = require('koa-router')
const mysql = require('mysql2/promise')
const render = require('koa-ejs')
const path = require('path')

const app =  new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'table',
    viewExt: 'ejs',
    cache: false
})
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password:'',
    database: 'day16'
})

const userModel =  require('./models/instuctor');
const userTemp = require('./controllers/instructor');
const userController = userTemp(userModel, pool);

const userModel2 = require('./models/course');
const userTemp2 = require('./controllers/course')
const userController2 = userTemp2(userModel2, pool)

router.get('/instructor/find_all', userController.findUserAll)
router.get('/instructor/find_by_id/:id', userController.findUserById)
router.get('/course/find_by_id/:id', userController2.findUserByIdCourses)
router.get('/course/find_by_price/:price', userController2.findUserByPrice)

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)