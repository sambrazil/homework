module.exports = function (userModel2, pool){
    return{
       async findUserByIdCourses(ctx, next){
           const userObject = await userModel2.findByIdCourses(pool, ctx.params.id)
           await ctx.render('course',{'course':userObject})
           await next()
       }, 
      async findUserByPrice(ctx, next){
         const userObject = await userModel2.findByPrice(pool, ctx.params.price)
         await ctx.render('course',{'course':userObject})
         await next()
      }
    }
  }