module.exports = function (userModel, pool){
    return{
        async findUserAll(ctx, next){
            const userObject = await userModel.findAll(pool)
            await ctx.render('instructor',{'instructor':userObject})
            await next()
        },
        async findUserById(ctx, next){
            const userObject = await userModel.findById(pool, ctx.params.id)
            await ctx.render('instructor',{'instructor':userObject})
            await next()
        }
      }
    }
  