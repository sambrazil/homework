const Koa = require('koa')
const Router = require('koa-router')
const mysql = require('mysql2/promise')

const app =  new Koa()
const router = new Router()

const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'day16'
})
router.get('/instructor/find_all', async(ctx,next)=>{
    const[rows]  = await pool.query('select * from instructors')
    ctx.body=rows
    await next()
})
router.get('/instructor/find_by_id/:id', async(ctx,next)=>{
    const[rows]  = await pool.query('select * from instructors where id = ?',[ctx.params.id])
    ctx.body=rows
    await next()
})
router.get('/course/find_by_id/:id', async(ctx,next)=>{
    const[rows]  = await pool.query('select * from courses where id = ?',[ctx.params.id])
    ctx.body=rows
    await next()
})
router.get('/course/find_by_price/:price', async(ctx,next)=>{
    const[rows]  = await pool.query('select * from courses where price = ?',[ctx.params.price])
    ctx.body=rows
    await next()
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)