1.นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร และจำนวนกี่คอร์ส
(ให้ใช้แค่ statement เดียวเท่านั้น)
select s.id as student_id, s.name as student_name, sum(c.price), count(e.course_id) from students s 
inner join enrolls e on s.id = e.student_id 
inner join courses c on e.course_id = c.id
group by e.student_id;

2.นักเรียนแต่ละคน ซื้อคอร์สไหน ราคาแพงสุด
select s.id, s.name, max(c.price) from students s 
inner join enrolls e on s.id = e.student_id
inner join courses c on e.course_id = c.id
group by e.student_id;

3.นักเรียนแต่ละคนซื้อคอร์สราคาเฉลี่ยคนละเท่าไหร่
select s.id, s.name, avg(c.price) from students s 
inner join enrolls e on s.id = e.student_id
inner join courses c on e.course_id = c.id
group by e.student_id;

