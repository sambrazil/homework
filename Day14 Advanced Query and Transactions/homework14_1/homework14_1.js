const Koa = require('koa')
const Router =  require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'homework14_1',
    viewExt: 'ejs',
    cache: false,
  })
 
router.get('/',async ctx =>{
   const connection = await mysql.createConnection({
      host:'localhost',
      user:'root',
      password:'',
      database:'day13'
    })
  const[sum]  = await connection.query('select sum(c.price) as sum from enrolls e inner join courses c on e.course_id = c.id') 
    //select sum(c.price) as sum from enrolls e inner join courses c on e.course_id = c.id;
  const[student,fields2] =await connection.query('select s.id, s.name, sum(c.price) as sum from students s inner join enrolls e on s.id = e.student_id inner join courses c on e.course_id = c.id group by e.student_id')
    //select s.id, s.name, sum(c.price) as sum from students s inner join enrolls e on s.id = e.student_id inner join courses c on e.course_id = c.id group by e.student_id;
  const dataEJS ={'data':sum,'data2':student}
 await ctx.render('homework14_1',dataEJS) 
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)