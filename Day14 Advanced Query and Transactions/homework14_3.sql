สร้าง View ชื่อ enroll_details ให้มี Requirement ดังนี้ (มีตัวอย่างในหน้าถัดไป)
มี column ดังนี้
student_name = ชื่อนักเรียนที่ลงคอร์ส
course_name = ชื่อคอร์ส
detail = รายละเอียดคอร์ส
price = ราคาคอร์ส
instructors = ชื่อผู้สอน
row สุดท้ายแสดงราคา Total ของคอร์สที่ขายได้
หากคอร์สที่ผู้เรียนลงไม่มีคนสอนให้เขียนว่า No Instructor

create view enroll_details as
select s.name as student_name, c.name as course_name, c.detail, c.price, coalesce(i.name,'No Instructor') as instructors from students s
       inner join enrolls e on s.id = e.student_id
       inner join courses c on e.course_id = c.id
       left join instructors i on c.teach_by = i.id
union select '','','Total', sum(c.price),''  from students s
       inner join enrolls e on s.id = e.student_id
       inner join courses c on e.course_id = c.id;
       
select*from enroll_details;

drop view enroll_details;
